import { createBrowserRouter } from 'react-router-dom';

import PrivateLayout from '@/layouts/PrivateLayout';
import RootLayout from '@/layouts/RootLayout';

import ErrorPage from '@/pages/ErrorPage';
import LoginPage from '@/pages/LoginPage';
import TodoPage from '@/pages/TodoPage';

export const router = createBrowserRouter([
	{
		path: '/',
		element: <RootLayout />,
		errorElement: <ErrorPage />,
		children: [
			{
				index: true,
				element: <LoginPage />,
			},
			{
				path: 'todos',
				element: <PrivateLayout />,
				children: [
					{
						index: true,
						element: <TodoPage />,
					},
				],
			},
		],
	},
]);
