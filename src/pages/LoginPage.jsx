import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Alert, Button, TextField } from '@mui/material';
import { Formik } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';

import { login } from '@/config/firebase';

import { useUserContext } from '@/context/UserContext';

import { useRedirectActiveUser } from '@/hooks/useRedirectActiveUser';

const loginSchema = Yup.object().shape({
	email: Yup.string().email('Email inválido').required('Campo requerido'),
	password: Yup.string().required('Campo requerido'),
});

const handlerError = {
	'auth/invalid-login-credentials': 'Credenciales incorrectas',
	'auth/too-many-requests':
		'El servicio no está disponible, por favor, intenténtalo de nuevo más tarde',
};

const LoginPage = () => {
	const { user } = useUserContext();
	const [error, setError] = useState();

	useRedirectActiveUser(user, '/todos');

	const onSubmit = async (user) => {
		try {
			await login(user);
		} catch (error) {
			console.error('Error login', error.code);
			setError(handlerError[error.code]);
		}
	};

	return (
		<>
			<div className="flex h-[100vh] flex-col items-center justify-center bg-gray-100">
				<div className="w-80 border bg-white p-4 shadow">
					<div className="flex flex-col items-center gap-1">
						<AccountCircleIcon sx={{ fontSize: 80, color: '#1976D2' }} />
						<h1 className="text-3xl">Login</h1>
					</div>
					<Formik
						initialValues={{ email: '', password: '' }}
						validationSchema={loginSchema}
						onSubmit={onSubmit}
					>
						{({
							values,
							errors,
							touched,
							isSubmitting,
							handleSubmit,
							handleChange,
							handleBlur,
						}) => (
							<form
								className="mt-6 flex flex-col gap-6"
								onSubmit={handleSubmit}
							>
								<TextField
									className="w-full"
									variant="outlined"
									label="Email"
									name="email"
									value={values.email}
									error={errors.email && touched.email}
									helperText={errors.email}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<TextField
									className="w-full"
									variant="outlined"
									label="Contraseña"
									name="password"
									type="password"
									value={values.password}
									error={errors.password && touched.password}
									helperText={errors.password}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<Button
									className="w-full"
									type="submit"
									variant="contained"
									disabled={isSubmitting}
								>
									Iniciar sesión
								</Button>
							</form>
						)}
					</Formik>
					{error && (
						<Alert severity="error" className="mt-2">
							{error}
						</Alert>
					)}
				</div>
			</div>
		</>
	);
};

export default LoginPage;
