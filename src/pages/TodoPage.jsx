import LogoutIcon from '@mui/icons-material/Logout';
import { useState } from 'react';

import Header from '@/components/Header';
import TodoCreate from '@/components/TodoCreate';
import TodoFilter from '@/components/TodoFilter';
import TodoInfo from '@/components/TodoInfo';
import TodoList from '@/components/TodoList';

import { logout } from '@/config/firebase';

const initialState = [
	{
		id: '1',
		text: 'Completed online JavaScript course',
		completed: true,
	},
	{
		id: '2',
		text: '10 minutes meditation',
		completed: false,
	},
	{
		id: '3',
		text: 'Read for 1 hour',
		completed: false,
	},
];

const reorder = (list, startIndex, endIndex) => {
	const result = [...list];
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};

const TodoPage = () => {
	const [todos, setTodos] = useState(initialState);
	const [filter, setFilter] = useState('all');

	const itemsLeft = todos.filter((todo) => !todo.completed).length;

	const addTodo = (text) => {
		const newTodo = {
			id: new Date().valueOf + todos.length,
			text,
			completed: false,
		};

		setTodos([...todos, newTodo]);
	};

	const updateTodo = (id) => {
		setTodos(
			todos.map((todo) => {
				if (todo.id === id) {
					return {
						...todo,
						completed: !todo.completed,
					};
				}

				return todo;
			})
		);
	};

	const removeTodo = (id) => {
		setTodos(todos.filter((todo) => todo.id !== id));
	};

	const removeAllCompleted = (id) => {
		setTodos(todos.filter((todo) => !todo.completed));
	};

	const filterTodos = () => {
		const filterHandler = {
			all: todos,
			active: todos.filter((todo) => !todo.completed),
			completed: todos.filter((todo) => todo.completed),
		};

		return filterHandler[filter];
	};

	const changeFilter = (filter) => {
		setFilter(filter);
	};

	const handlerLogout = async () => {
		try {
			await logout();
		} catch (error) {
			console.error('Error logout', error);
		}
	};

	const handleDragEnd = (result) => {
		const { destination, source } = result;
		if (!destination) return;
		if (
			source.index === destination.index &&
			source.droppableId === destination.droppableId
		)
			return;

		setTodos((prevTasks) =>
			reorder(prevTasks, source.index, destination.index)
		);
	};

	return (
		<div className="min-h-screen bg-gray-100 dark:bg-gray-900">
			<Header />
			<main className="container relative bottom-[10rem] mx-auto max-w-lg  space-y-4 px-4 sm:bottom-[8rem]">
				<div className="space-y-4">
					<TodoCreate addTodo={addTodo} />
					<div>
						<TodoList
							todos={filterTodos()}
							updateTodo={updateTodo}
							removeTodo={removeTodo}
							handleDragEnd={handleDragEnd}
						/>
						<TodoInfo
							itemsLeft={itemsLeft}
							removeAllCompleted={removeAllCompleted}
						/>
					</div>
				</div>
				<TodoFilter filter={filter} changeFilter={changeFilter} />
				<div
					className="flex cursor-pointer items-center justify-center gap-2"
					onClick={handlerLogout}
				>
					<span className={`hover:text-blue-60 text-blue-600`}>Logout</span>
					<LogoutIcon sx={{ fontSize: 20, color: '#2563EB' }} />
				</div>
			</main>
		</div>
	);
};

export default TodoPage;
