import { useRouteError } from 'react-router-dom';

const ErrorPage = () => {
	const { status, statusText } = useRouteError();

	return (
		<>
			<div className="p-6 text-center">
				<h1>{status}</h1>
				<p>{statusText}</p>
			</div>
		</>
	);
};

export default ErrorPage;
