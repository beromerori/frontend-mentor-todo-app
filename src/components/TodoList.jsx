import { DragDropContext, Draggable, Droppable } from '@hello-pangea/dnd';

import TodoItem from './TodoItem';

const TodoList = ({ todos, updateTodo, removeTodo, handleDragEnd }) => {
	return (
		<DragDropContext onDragEnd={handleDragEnd}>
			<Droppable droppableId="todos">
				{(droppableProvider) => (
					<div
						className="overflow-hidden rounded-t-md bg-white shadow dark:bg-gray-700 [&>div]:border-b [&>div]:p-4 dark:[&>div]:border-gray-600"
						ref={droppableProvider.innerRef}
						{...droppableProvider.droppableProps}
					>
						{!todos.length && (
							<div className="text-center text-sm italic dark:text-gray-300">
								No todos
							</div>
						)}
						{todos.map((todo, index) => (
							<Draggable key={todo.id} index={index} draggableId={`${todo.id}`}>
								{(draggableProvider) => (
									<TodoItem
										ref={draggableProvider.innerRef}
										{...draggableProvider.droppableProps}
										{...draggableProvider.dragHandleProps}
										todo={todo}
										updateTodo={updateTodo}
										removeTodo={removeTodo}
									/>
								)}
							</Draggable>
						))}
						{droppableProvider.placeholder}
					</div>
				)}
			</Droppable>
		</DragDropContext>
	);
};

export default TodoList;
