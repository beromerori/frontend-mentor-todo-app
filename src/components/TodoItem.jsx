import React from 'react';

import IconCheck from './icons/IconCheck';
import IconCross from './icons/IconCross';

const TodoItem = React.forwardRef(
	({ todo, updateTodo, removeTodo, ...props }, ref) => {
		const { id, text, completed } = todo;

		return (
			<div
				className={`flex cursor-pointer gap-4 text-sm dark:bg-gray-700 
					${completed ? 'dark:text-white' : 'text-[#6B7280]'}`}
				ref={ref}
				{...props}
			>
				<button
					className={`grid h-5 w-5 flex-none place-items-center rounded-full border ${completed && 'bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500'}`}
					onClick={() => updateTodo(id)}
				>
					<IconCheck></IconCheck>
				</button>
				<p className={`grow ${completed && 'line-through'}`}>{text}</p>
				<button className="flex-none" onClick={() => removeTodo(id)}>
					<IconCross></IconCross>
				</button>
			</div>
		);
	}
);

/*const TodoItem = ({ todo, updateTodo, removeTodo }) => {
	
	const { id, text, completed } = todo;

	return (
		<div className="flex gap-4 text-sm text-gray-500">
			<button
				className={`grid h-5 w-5 flex-none place-items-center rounded-full border ${completed && 'bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500'}`}
				onClick={() => updateTodo(id)}
			>
				<IconCheck></IconCheck>
			</button>
			<p className={`grow ${completed && 'line-through'}`}>{text}</p>
			<button className="flex-none" onClick={() => removeTodo(id)}>
				<IconCross></IconCross>
			</button>
		</div>
	);
};*/

export default TodoItem;
