const TodoInfo = ({ itemsLeft, removeAllCompleted }) => {
	return (
		<div className="flex justify-between gap-2 rounded-b-md bg-white p-4 text-xs shadow dark:bg-gray-700 dark:text-white">
			<div>{itemsLeft} items left</div>
			<div className="cursor-pointer" onClick={() => removeAllCompleted()}>
				Clear Completed
			</div>
		</div>
	);
};

export default TodoInfo;
