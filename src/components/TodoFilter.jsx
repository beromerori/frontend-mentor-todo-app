const TodoFilter = ({ filter, changeFilter }) => {
	return (
		<div className="text-grey-100 flex cursor-pointer justify-center gap-2 rounded bg-white p-4 text-xs shadow dark:bg-gray-700 dark:text-white">
			<span
				className={`hover:text-blue-600 ${filter === 'all' && 'text-blue-600'}`}
				onClick={() => changeFilter('all')}
			>
				All
			</span>
			<span
				className={`hover:text-blue-600 ${filter === 'active' && 'text-blue-600'}`}
				onClick={() => changeFilter('active')}
			>
				Active
			</span>
			<span
				className={`hover:text-blue-600 ${filter === 'completed' && 'text-blue-600'}`}
				onClick={() => changeFilter('completed')}
			>
				Completed
			</span>
		</div>
	);
};

export default TodoFilter;
