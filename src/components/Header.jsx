import { useEffect, useState } from 'react';

import IconMoon from './icons/IconMoon';
import IconSun from './icons/IconSun';

const Header = () => {
	const checkInitialDarkMode = localStorage.getItem('theme') === 'dark';

	const [isDarkMode, setDarkMode] = useState(checkInitialDarkMode);

	const onChangeMode = () => {
		setDarkMode(!isDarkMode);
	};

	useEffect(() => {
		if (isDarkMode) {
			localStorage.setItem('theme', 'dark');
			document.documentElement.classList.add('dark');
			return;
		}

		localStorage.setItem('theme', 'light');
		document.documentElement.classList.remove('dark');
	}, [isDarkMode]);

	return (
		<div
			className="h-64
				bg-[url('./assets/images/bg-mobile-light.jpg')] bg-cover 
				bg-center bg-no-repeat
				sm:bg-[url('./assets/images/bg-desktop-light.jpg')]
				dark:bg-[url('./assets/images/bg-mobile-dark.jpg')]
				sm:dark:bg-[url('./assets/images/bg-desktop-dark.jpg')]"
		>
			<div className="container mx-auto flex max-w-lg justify-between px-4 pt-[2rem] sm:pt-[4rem]">
				<h1 className="text-3xl font-bold tracking-[.25em] text-white">TODO</h1>
				<button className="cursor-pointer" onClick={onChangeMode}>
					{isDarkMode ? <IconSun /> : <IconMoon />}
				</button>
			</div>
		</div>
	);
};

export default Header;
