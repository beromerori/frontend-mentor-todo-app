import { useState } from 'react';

const TodoCreate = ({ addTodo }) => {
	const [text, setText] = useState('');

	const handlerAddTodo = (e) => {
		e.preventDefault();

		if (!text.trim()) {
			return setText('');
		}

		addTodo(text);
		setText('');
	};

	return (
		<form
			onSubmit={handlerAddTodo}
			className="flex items-center gap-4 rounded bg-white p-4 shadow dark:bg-slate-700"
		>
			<button className="h-5 w-full max-w-5 rounded-full border"></button>
			<input
				className="w-full outline-none dark:bg-slate-700 dark:text-gray-300"
				placeholder="Create a new todo..."
				type="text"
				value={text}
				onChange={(e) => {
					setText(e.target.value);
				}}
			/>
		</form>
	);
};

export default TodoCreate;
